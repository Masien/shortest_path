/********************************************************************************
** Form generated from reading UI file 'testVS.ui'
**
** Created by: Qt User Interface Compiler version 6.2.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TESTVS_H
#define UI_TESTVS_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGraphicsView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_testVSClass
{
public:
    QPushButton *Wroclaw_Btn;
    QLabel *miasto_p;
    QGraphicsView *graphicsView;
    QPushButton *Poznan_Btn;
    QPushButton *Szczecin_Btn;
    QPushButton *ZielonaG_Btn;
    QPushButton *Gdansk_Btn;
    QPushButton *Warszawa_Btn;
    QPushButton *Rzeszow_Btn;
    QPushButton *Krakow_Btn;
    QPushButton *Lublin_Btn;
    QPushButton *Bialystok_Btn;
    QPushButton *Olsztyn_Btn;
    QPushButton *Katowice_Btn;
    QPushButton *Kielce_Btn;
    QPushButton *Bydgoszcz_Btn;
    QPushButton *Lodz_Btn;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *miasto_k;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout;
    QRadioButton *radioButton;
    QRadioButton *radioButton_2;
    QPushButton *Dij_Btn;
    QLabel *label;
    QLabel *Dij_label;
    QPushButton *Opole_Btn;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QLabel *label_9;
    QLabel *label_10;
    QLabel *label_11;
    QLabel *label_12;
    QLabel *label_13;
    QLabel *label_14;
    QLabel *label_15;
    QLabel *label_16;
    QLabel *label_17;
    QLabel *label_18;
    QLabel *label_19;
    QLabel *label_20;
    QLabel *label_21;
    QLabel *label_22;
    QLabel *label_23;
    QLabel *label_24;
    QLabel *label_25;
    QLabel *label_26;
    QLabel *label_27;
    QLabel *label_28;
    QLabel *label_29;
    QLabel *label_30;
    QLabel *label_31;
    QLabel *label_32;
    QLabel *label_33;
    QLabel *label_34;

    void setupUi(QWidget *testVSClass)
    {
        if (testVSClass->objectName().isEmpty())
            testVSClass->setObjectName(QString::fromUtf8("testVSClass"));
        testVSClass->resize(852, 607);
        Wroclaw_Btn = new QPushButton(testVSClass);
        Wroclaw_Btn->setObjectName(QString::fromUtf8("Wroclaw_Btn"));
        Wroclaw_Btn->setGeometry(QRect(130, 380, 31, 24));
        miasto_p = new QLabel(testVSClass);
        miasto_p->setObjectName(QString::fromUtf8("miasto_p"));
        miasto_p->setGeometry(QRect(640, 90, 101, 16));
        graphicsView = new QGraphicsView(testVSClass);
        graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
        graphicsView->setGeometry(QRect(0, 0, 631, 601));
        Poznan_Btn = new QPushButton(testVSClass);
        Poznan_Btn->setObjectName(QString::fromUtf8("Poznan_Btn"));
        Poznan_Btn->setGeometry(QRect(190, 260, 31, 24));
        Szczecin_Btn = new QPushButton(testVSClass);
        Szczecin_Btn->setObjectName(QString::fromUtf8("Szczecin_Btn"));
        Szczecin_Btn->setGeometry(QRect(40, 100, 31, 24));
        ZielonaG_Btn = new QPushButton(testVSClass);
        ZielonaG_Btn->setObjectName(QString::fromUtf8("ZielonaG_Btn"));
        ZielonaG_Btn->setGeometry(QRect(40, 250, 31, 24));
        Gdansk_Btn = new QPushButton(testVSClass);
        Gdansk_Btn->setObjectName(QString::fromUtf8("Gdansk_Btn"));
        Gdansk_Btn->setGeometry(QRect(270, 40, 31, 24));
        Warszawa_Btn = new QPushButton(testVSClass);
        Warszawa_Btn->setObjectName(QString::fromUtf8("Warszawa_Btn"));
        Warszawa_Btn->setGeometry(QRect(390, 250, 41, 24));
        Rzeszow_Btn = new QPushButton(testVSClass);
        Rzeszow_Btn->setObjectName(QString::fromUtf8("Rzeszow_Btn"));
        Rzeszow_Btn->setGeometry(QRect(520, 470, 21, 24));
        Krakow_Btn = new QPushButton(testVSClass);
        Krakow_Btn->setObjectName(QString::fromUtf8("Krakow_Btn"));
        Krakow_Btn->setGeometry(QRect(380, 490, 21, 24));
        Lublin_Btn = new QPushButton(testVSClass);
        Lublin_Btn->setObjectName(QString::fromUtf8("Lublin_Btn"));
        Lublin_Btn->setGeometry(QRect(530, 370, 21, 24));
        Bialystok_Btn = new QPushButton(testVSClass);
        Bialystok_Btn->setObjectName(QString::fromUtf8("Bialystok_Btn"));
        Bialystok_Btn->setGeometry(QRect(520, 130, 21, 24));
        Olsztyn_Btn = new QPushButton(testVSClass);
        Olsztyn_Btn->setObjectName(QString::fromUtf8("Olsztyn_Btn"));
        Olsztyn_Btn->setGeometry(QRect(430, 110, 21, 24));
        Katowice_Btn = new QPushButton(testVSClass);
        Katowice_Btn->setObjectName(QString::fromUtf8("Katowice_Btn"));
        Katowice_Btn->setGeometry(QRect(280, 440, 31, 24));
        Kielce_Btn = new QPushButton(testVSClass);
        Kielce_Btn->setObjectName(QString::fromUtf8("Kielce_Btn"));
        Kielce_Btn->setGeometry(QRect(410, 400, 21, 24));
        Bydgoszcz_Btn = new QPushButton(testVSClass);
        Bydgoszcz_Btn->setObjectName(QString::fromUtf8("Bydgoszcz_Btn"));
        Bydgoszcz_Btn->setGeometry(QRect(280, 170, 21, 24));
        Lodz_Btn = new QPushButton(testVSClass);
        Lodz_Btn->setObjectName(QString::fromUtf8("Lodz_Btn"));
        Lodz_Btn->setGeometry(QRect(310, 330, 21, 24));
        label_2 = new QLabel(testVSClass);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(640, 60, 201, 31));
        QFont font;
        font.setPointSize(16);
        font.setBold(true);
        label_2->setFont(font);
        label_3 = new QLabel(testVSClass);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(640, 120, 191, 21));
        label_3->setFont(font);
        miasto_k = new QLabel(testVSClass);
        miasto_k->setObjectName(QString::fromUtf8("miasto_k"));
        miasto_k->setGeometry(QRect(640, 150, 91, 16));
        layoutWidget = new QWidget(testVSClass);
        layoutWidget->setObjectName(QString::fromUtf8("layoutWidget"));
        layoutWidget->setGeometry(QRect(640, 190, 128, 48));
        verticalLayout = new QVBoxLayout(layoutWidget);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalLayout->setContentsMargins(0, 0, 0, 0);
        radioButton = new QRadioButton(layoutWidget);
        radioButton->setObjectName(QString::fromUtf8("radioButton"));

        verticalLayout->addWidget(radioButton);

        radioButton_2 = new QRadioButton(layoutWidget);
        radioButton_2->setObjectName(QString::fromUtf8("radioButton_2"));

        verticalLayout->addWidget(radioButton_2);

        Dij_Btn = new QPushButton(testVSClass);
        Dij_Btn->setObjectName(QString::fromUtf8("Dij_Btn"));
        Dij_Btn->setGeometry(QRect(640, 280, 101, 31));
        label = new QLabel(testVSClass);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(640, 320, 181, 31));
        label->setFont(font);
        Dij_label = new QLabel(testVSClass);
        Dij_label->setObjectName(QString::fromUtf8("Dij_label"));
        Dij_label->setGeometry(QRect(640, 360, 49, 16));
        Opole_Btn = new QPushButton(testVSClass);
        Opole_Btn->setObjectName(QString::fromUtf8("Opole_Btn"));
        Opole_Btn->setGeometry(QRect(200, 410, 31, 24));
        label_5 = new QLabel(testVSClass);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(540, 250, 49, 16));
        label_6 = new QLabel(testVSClass);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(460, 190, 49, 16));
        label_7 = new QLabel(testVSClass);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(470, 110, 49, 16));
        label_8 = new QLabel(testVSClass);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(530, 430, 49, 16));
        label_9 = new QLabel(testVSClass);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(470, 310, 49, 16));
        label_10 = new QLabel(testVSClass);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(460, 380, 49, 16));
        label_11 = new QLabel(testVSClass);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(460, 490, 49, 16));
        label_12 = new QLabel(testVSClass);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(450, 440, 49, 16));
        label_13 = new QLabel(testVSClass);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        label_13->setGeometry(QRect(390, 450, 49, 16));
        label_14 = new QLabel(testVSClass);
        label_14->setObjectName(QString::fromUtf8("label_14"));
        label_14->setGeometry(QRect(340, 480, 49, 16));
        label_15 = new QLabel(testVSClass);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        label_15->setGeometry(QRect(400, 330, 49, 16));
        label_16 = new QLabel(testVSClass);
        label_16->setObjectName(QString::fromUtf8("label_16"));
        label_16->setGeometry(QRect(350, 290, 49, 16));
        label_17 = new QLabel(testVSClass);
        label_17->setObjectName(QString::fromUtf8("label_17"));
        label_17->setGeometry(QRect(400, 200, 49, 16));
        label_18 = new QLabel(testVSClass);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        label_18->setGeometry(QRect(340, 140, 49, 16));
        label_19 = new QLabel(testVSClass);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        label_19->setGeometry(QRect(290, 250, 49, 16));
        label_20 = new QLabel(testVSClass);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        label_20->setGeometry(QRect(270, 110, 49, 16));
        label_21 = new QLabel(testVSClass);
        label_21->setObjectName(QString::fromUtf8("label_21"));
        label_21->setGeometry(QRect(340, 420, 49, 16));
        label_22 = new QLabel(testVSClass);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        label_22->setGeometry(QRect(290, 390, 49, 16));
        label_23 = new QLabel(testVSClass);
        label_23->setObjectName(QString::fromUtf8("label_23"));
        label_23->setGeometry(QRect(250, 430, 49, 16));
        label_24 = new QLabel(testVSClass);
        label_24->setObjectName(QString::fromUtf8("label_24"));
        label_24->setGeometry(QRect(170, 410, 49, 16));
        label_25 = new QLabel(testVSClass);
        label_25->setObjectName(QString::fromUtf8("label_25"));
        label_25->setGeometry(QRect(160, 330, 49, 16));
        label_26 = new QLabel(testVSClass);
        label_26->setObjectName(QString::fromUtf8("label_26"));
        label_26->setGeometry(QRect(240, 220, 49, 16));
        label_27 = new QLabel(testVSClass);
        label_27->setObjectName(QString::fromUtf8("label_27"));
        label_27->setGeometry(QRect(350, 370, 49, 16));
        label_28 = new QLabel(testVSClass);
        label_28->setObjectName(QString::fromUtf8("label_28"));
        label_28->setGeometry(QRect(130, 170, 49, 16));
        label_29 = new QLabel(testVSClass);
        label_29->setObjectName(QString::fromUtf8("label_29"));
        label_29->setGeometry(QRect(40, 180, 49, 16));
        label_30 = new QLabel(testVSClass);
        label_30->setObjectName(QString::fromUtf8("label_30"));
        label_30->setGeometry(QRect(70, 330, 49, 16));
        label_31 = new QLabel(testVSClass);
        label_31->setObjectName(QString::fromUtf8("label_31"));
        label_31->setGeometry(QRect(140, 60, 49, 16));
        label_32 = new QLabel(testVSClass);
        label_32->setObjectName(QString::fromUtf8("label_32"));
        label_32->setGeometry(QRect(350, 70, 49, 16));
        label_33 = new QLabel(testVSClass);
        label_33->setObjectName(QString::fromUtf8("label_33"));
        label_33->setGeometry(QRect(100, 250, 49, 16));
        label_34 = new QLabel(testVSClass);
        label_34->setObjectName(QString::fromUtf8("label_34"));
        label_34->setGeometry(QRect(250, 310, 49, 16));
        layoutWidget->raise();
        miasto_p->raise();
        graphicsView->raise();
        Wroclaw_Btn->raise();
        Poznan_Btn->raise();
        Szczecin_Btn->raise();
        ZielonaG_Btn->raise();
        Gdansk_Btn->raise();
        Warszawa_Btn->raise();
        Rzeszow_Btn->raise();
        Krakow_Btn->raise();
        Lublin_Btn->raise();
        Bialystok_Btn->raise();
        Olsztyn_Btn->raise();
        Katowice_Btn->raise();
        Kielce_Btn->raise();
        Bydgoszcz_Btn->raise();
        Lodz_Btn->raise();
        label_2->raise();
        label_3->raise();
        miasto_k->raise();
        Dij_Btn->raise();
        label->raise();
        Dij_label->raise();
        Opole_Btn->raise();
        label_5->raise();
        label_6->raise();
        label_7->raise();
        label_8->raise();
        label_9->raise();
        label_10->raise();
        label_11->raise();
        label_12->raise();
        label_13->raise();
        label_14->raise();
        label_15->raise();
        label_16->raise();
        label_17->raise();
        label_18->raise();
        label_19->raise();
        label_20->raise();
        label_21->raise();
        label_22->raise();
        label_23->raise();
        label_24->raise();
        label_25->raise();
        label_26->raise();
        label_27->raise();
        label_28->raise();
        label_29->raise();
        label_30->raise();
        label_31->raise();
        label_32->raise();
        label_33->raise();
        label_34->raise();

        retranslateUi(testVSClass);

        QMetaObject::connectSlotsByName(testVSClass);
    } // setupUi

    void retranslateUi(QWidget *testVSClass)
    {
        testVSClass->setWindowTitle(QCoreApplication::translate("testVSClass", "testVS", nullptr));
        Wroclaw_Btn->setText(QCoreApplication::translate("testVSClass", "Wr", nullptr));
        miasto_p->setText(QCoreApplication::translate("testVSClass", "Wybierz miasto", nullptr));
        Poznan_Btn->setText(QCoreApplication::translate("testVSClass", "Po", nullptr));
        Szczecin_Btn->setText(QCoreApplication::translate("testVSClass", "Sz", nullptr));
        ZielonaG_Btn->setText(QCoreApplication::translate("testVSClass", "Z.G", nullptr));
        Gdansk_Btn->setText(QCoreApplication::translate("testVSClass", "Gd", nullptr));
        Warszawa_Btn->setText(QCoreApplication::translate("testVSClass", "Wwa", nullptr));
        Rzeszow_Btn->setText(QCoreApplication::translate("testVSClass", "Rz", nullptr));
        Krakow_Btn->setText(QCoreApplication::translate("testVSClass", "Kr", nullptr));
        Lublin_Btn->setText(QCoreApplication::translate("testVSClass", "Lu", nullptr));
        Bialystok_Btn->setText(QCoreApplication::translate("testVSClass", "Bi", nullptr));
        Olsztyn_Btn->setText(QCoreApplication::translate("testVSClass", "Ol", nullptr));
        Katowice_Btn->setText(QCoreApplication::translate("testVSClass", "Kat", nullptr));
        Kielce_Btn->setText(QCoreApplication::translate("testVSClass", "Ki", nullptr));
        Bydgoszcz_Btn->setText(QCoreApplication::translate("testVSClass", "By", nullptr));
        Lodz_Btn->setText(QCoreApplication::translate("testVSClass", "\305\201\303\263", nullptr));
        label_2->setText(QCoreApplication::translate("testVSClass", "Miasto pocz\304\205tkowe:", nullptr));
        label_3->setText(QCoreApplication::translate("testVSClass", "Miasto ko\305\204cowe:", nullptr));
        miasto_k->setText(QCoreApplication::translate("testVSClass", "Wybierz miasto", nullptr));
        radioButton->setText(QCoreApplication::translate("testVSClass", "Miasto pocz\304\205tkowe", nullptr));
        radioButton_2->setText(QCoreApplication::translate("testVSClass", "Miasto ko\305\204cowe", nullptr));
        Dij_Btn->setText(QCoreApplication::translate("testVSClass", "Oblicz droge", nullptr));
        label->setText(QCoreApplication::translate("testVSClass", "Najkr\303\263tsza droga:", nullptr));
        Dij_label->setText(QCoreApplication::translate("testVSClass", "TextLabel", nullptr));
        Opole_Btn->setText(QCoreApplication::translate("testVSClass", "Op", nullptr));
        label_5->setText(QCoreApplication::translate("testVSClass", "246", nullptr));
        label_6->setText(QCoreApplication::translate("testVSClass", "200", nullptr));
        label_7->setText(QCoreApplication::translate("testVSClass", "223", nullptr));
        label_8->setText(QCoreApplication::translate("testVSClass", "184", nullptr));
        label_9->setText(QCoreApplication::translate("testVSClass", "173", nullptr));
        label_10->setText(QCoreApplication::translate("testVSClass", "195", nullptr));
        label_11->setText(QCoreApplication::translate("testVSClass", "172", nullptr));
        label_12->setText(QCoreApplication::translate("testVSClass", "155", nullptr));
        label_13->setText(QCoreApplication::translate("testVSClass", "114", nullptr));
        label_14->setText(QCoreApplication::translate("testVSClass", "80", nullptr));
        label_15->setText(QCoreApplication::translate("testVSClass", "181", nullptr));
        label_16->setText(QCoreApplication::translate("testVSClass", "142", nullptr));
        label_17->setText(QCoreApplication::translate("testVSClass", "214", nullptr));
        label_18->setText(QCoreApplication::translate("testVSClass", "224", nullptr));
        label_19->setText(QCoreApplication::translate("testVSClass", "226", nullptr));
        label_20->setText(QCoreApplication::translate("testVSClass", "221", nullptr));
        label_21->setText(QCoreApplication::translate("testVSClass", "156", nullptr));
        label_22->setText(QCoreApplication::translate("testVSClass", "203", nullptr));
        label_23->setText(QCoreApplication::translate("testVSClass", "104", nullptr));
        label_24->setText(QCoreApplication::translate("testVSClass", "97", nullptr));
        label_25->setText(QCoreApplication::translate("testVSClass", "182", nullptr));
        label_26->setText(QCoreApplication::translate("testVSClass", "138", nullptr));
        label_27->setText(QCoreApplication::translate("testVSClass", "153", nullptr));
        label_28->setText(QCoreApplication::translate("testVSClass", "265", nullptr));
        label_29->setText(QCoreApplication::translate("testVSClass", "213", nullptr));
        label_30->setText(QCoreApplication::translate("testVSClass", "166", nullptr));
        label_31->setText(QCoreApplication::translate("testVSClass", "358", nullptr));
        label_32->setText(QCoreApplication::translate("testVSClass", "166", nullptr));
        label_33->setText(QCoreApplication::translate("testVSClass", "153", nullptr));
        label_34->setText(QCoreApplication::translate("testVSClass", "206", nullptr));
    } // retranslateUi

};

namespace Ui {
    class testVSClass: public Ui_testVSClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TESTVS_H
